import { AxiosRequestConfig } from "axios";
import {
  RequestEpisodeScraper,
  RequestEpisodeScraperHook
} from "@alline/scraper-request";
export interface JsonEpisodeScraperOption {
  axios?: AxiosRequestConfig;
}

export interface JsonEpisodeScraperHook
  extends RequestEpisodeScraperHook<any, any> {}

export class JsonEpisodeScraper extends RequestEpisodeScraper {
  constructor(option: JsonEpisodeScraperOption) {
    const { axios } = option;
    super({
      axios,
      transform: async value => value,
      label: "JsonEpisodeScraper"
    });
  }
}

# [3.0.0](https://gitlab.com/alline/scraper-json/compare/v2.0.0...v3.0.0) (2020-07-09)


### Bug Fixes

* update dependencies ([4c77bf9](https://gitlab.com/alline/scraper-json/commit/4c77bf940dd6240c6b664808b7e26e3893531f07))


### BREAKING CHANGES

* update @alline/core to v3

# [2.0.0](https://gitlab.com/alline/scraper-json/compare/v1.0.2...v2.0.0) (2020-07-02)


### Code Refactoring

* use RequestEpisodeScraper as base ([8c813c6](https://gitlab.com/alline/scraper-json/commit/8c813c66b4920d830f527d81d8e5c55ad2110180))


### BREAKING CHANGES

* hooks api changed

## [1.0.2](https://gitlab.com/alline/scraper-json/compare/v1.0.1...v1.0.2) (2020-06-29)


### Bug Fixes

* hook api ([c5b1ca1](https://gitlab.com/alline/scraper-json/commit/c5b1ca11ae9a53df77eced42d37ac5687c317aed))
* resolve promise ([f4afa23](https://gitlab.com/alline/scraper-json/commit/f4afa233fe15b11500486191871e35e395153971))

## [1.0.1](https://gitlab.com/alline/scraper-json/compare/v1.0.0...v1.0.1) (2020-06-29)


### Bug Fixes

* encode uri ([f720e1d](https://gitlab.com/alline/scraper-json/commit/f720e1da480793dcac8d460fe421b1ee98ace653))

# 1.0.0 (2020-06-29)


### Features

* initial commit ([197043e](https://gitlab.com/alline/scraper-json/commit/197043eded1e890d105b065e31d933131e1ea83b))
